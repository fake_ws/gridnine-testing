import com.gridnine.testing.filters.global.MoreThanAnyHoursOnFlightFilter;
import com.gridnine.testing.filters.global.MoreThanAnyTransferFilter;
import com.gridnine.testing.filters.ground.MoreThanAnyHoursOnGroundFilter;
import com.gridnine.testing.filters.time.MoreThanAnyHoursOnAirFilter;
import com.gridnine.testing.filters.valid.DepartAfterArrivalFilter;
import com.gridnine.testing.filters.valid.DepartPastFilter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class FiltersTest {
    @Test
    @DisplayName("Фильтр, возвращающий полёты, для сегментов которых дата и время вылета не ранее текущих")
    public void departPastFilterTest() {
        assertEquals(Samples.flightsWithoutDepartPast().toString(),
                new DepartPastFilter().filter(Samples.createFlightsForTest()).toString());
    }

    @Test
    @DisplayName("Фильтр, возвращающий полёты, для сегментов которых дата и время прилёта предыдущего сегмента не позднее даты и времени вылета следующего сегмента")
    public void departAfterArrivalFilterTest() {
        assertEquals(Samples.flightsWithoutDepartAfterArrival().toString(),
                new DepartAfterArrivalFilter().filter(Samples.createFlightsForTest()).toString());
    }

    @Test
    @DisplayName("Фильтр, возвращащий полёты, суммарное время между пересадками на которых составляет не более 2 часов")
    public void moreThanTwoHoursOnGroundFilterTest() {
        assertEquals(Samples.flightsWithoutMoreThanTwoHoursOnGround().toString(),
                new MoreThanAnyHoursOnGroundFilter().filter(Samples.createFlightsForTest()).toString());
    }

    @Test
    @DisplayName("Фильтр, возвращащий полёты, суммарное время в воздухе на которых составляет не более 3 часов")
    public void moreThanThreeHoursOnAirFilterTest() {
        assertEquals(Samples.flightsWithoutMoreThanThreeHoursOnAir().toString(),
                new MoreThanAnyHoursOnAirFilter().filter(3L, Samples.createFlightsForTest()).toString());
    }

    @Test
    @DisplayName("Фильтр, возвращащий полёты, число пересадок на которых не более 2")
    public void moreThanAnyTransferFilterTest() {
        assertEquals(Samples.flightsWithoutMoreThanTwoTransfers().toString(),
                new MoreThanAnyTransferFilter().filter(2L, Samples.createFlightsForTest()).toString());
    }

    @Test
    @DisplayName("Фильтр, возвращащий полёты, суммарное время (воздух и пересадки) на которых составляет не более 4 часов")
    public void moreThanAnyHoursOnFlightFilterTest() {
        assertEquals(Samples.flightsWithoutMoreThanFourHoursOnFlight().toString(),
                new MoreThanAnyHoursOnFlightFilter().filter(4L, Samples.createFlightsForTest()).toString());
    }
}
