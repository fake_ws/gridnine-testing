package com.gridnine.testing;

import com.gridnine.testing.filters.ground.MoreThanAnyHoursOnGroundFilter;
import com.gridnine.testing.filters.valid.DepartAfterArrivalFilter;
import com.gridnine.testing.filters.valid.DepartPastFilter;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Flight> flightList = FlightBuilder.createFlights();

        System.out.println("Стартовый набор данных: " + flightList);
        System.out.println("Исключая полёты с вылетом до текущего времени: " + new DepartPastFilter().filter(flightList));
        System.out.println("Исключая полёты с вылетом раньше прилёта : " + new DepartAfterArrivalFilter().filter(flightList));
        System.out.println("Исключая полёты с временем на земле более 2 часов: " + new MoreThanAnyHoursOnGroundFilter().filter(flightList));
    }
}
