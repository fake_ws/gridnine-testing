package com.gridnine.testing.filters;

import java.util.List;

public interface FilterWithParameter<T, V> {
    List<T> filter(V v, List<T> list);
}
