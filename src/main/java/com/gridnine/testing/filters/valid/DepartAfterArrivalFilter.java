package com.gridnine.testing.filters.valid;

import com.gridnine.testing.Flight;
import com.gridnine.testing.filters.Filter;

import java.util.List;
import java.util.stream.Collectors;

public class DepartAfterArrivalFilter implements Filter<Flight> {
    /*
    Группа фильтров, проверяющих валидность данных:
    фильтр, возвращающий полёты, для сегментов которых дата и время прилёта предыдущего сегмента
    не позднее даты и времени вылета следующего сегмента
     */
    @Override
    public List<Flight> filter(List<Flight> flightList) {
        return flightList.stream().filter(
                flight -> flight.getSegments().stream()
                        .anyMatch(segment -> segment.getDepartureDate().isBefore(segment.getArrivalDate()))
        ).collect(Collectors.toList());
    }
}
