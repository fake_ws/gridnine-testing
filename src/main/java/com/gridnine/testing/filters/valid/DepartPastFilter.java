package com.gridnine.testing.filters.valid;

import com.gridnine.testing.Flight;
import com.gridnine.testing.filters.Filter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class DepartPastFilter implements Filter<Flight> {
    /*
    Группа фильтров, проверяющих валидность данных:
    фильтр, возвращающий полёты, для сегментов которых дата и время вылета не ранее текущих
     */
    @Override
    public List<Flight> filter(List<Flight> flightList) {
        return flightList.stream().filter(
                flight -> flight.getSegments().stream()
                        .anyMatch(segment -> segment.getDepartureDate().isAfter(LocalDateTime.now()))
        ).collect(Collectors.toList());
    }
}
