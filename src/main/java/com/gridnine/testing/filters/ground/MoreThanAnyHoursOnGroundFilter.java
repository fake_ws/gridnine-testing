package com.gridnine.testing.filters.ground;

import com.gridnine.testing.Flight;
import com.gridnine.testing.Segment;
import com.gridnine.testing.filters.Filter;
import com.gridnine.testing.filters.FilterWithParameter;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class MoreThanAnyHoursOnGroundFilter implements Filter<Flight>, FilterWithParameter<Flight, Long> {
    /*
    Группа фильтров, работающих с информацией о нахождении пассажира на земле:
    - фильтр, возвращащий полёты, суммарное время между пересадками на которых составляет не более 2 часов;
    - фильтр, возвращащий полёты, суммарное время между пересадками на которых составляет не более N часов (упрощённый вариант ввода данных)
     */
    @Override
    public List<Flight> filter(List<Flight> flightList) {
        List<Flight> filteredList = new ArrayList<>();

        for (Flight flight : flightList) {
            List<Segment> segmentList = flight.getSegments();

            long time = 0;
            for (int i = 0; i < segmentList.size() - 1; i++) {
                Duration duration = Duration.between(segmentList.get(i + 1).getDepartureDate(), segmentList.get(i).getArrivalDate());
                time += Math.abs(duration.toHours());
            }
            if (time <= 2)
                filteredList.add(flight);
        }
        return filteredList;
    }

    @Override
    public List<Flight> filter(Long hours, List<Flight> flightList) {
        List<Flight> filteredList = new ArrayList<>();

        for (Flight flight : flightList) {
            List<Segment> segmentList = flight.getSegments();

            long time = 0;
            for (int i = 0; i < segmentList.size() - 1; i++) {
                Duration duration = Duration.between(segmentList.get(i + 1).getDepartureDate(), segmentList.get(i).getArrivalDate());
                time += Math.abs(duration.toHours());
            }
            if (time <= hours)
                filteredList.add(flight);
        }
        return filteredList;
    }
}
