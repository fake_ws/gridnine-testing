package com.gridnine.testing.filters.global;

import com.gridnine.testing.Flight;
import com.gridnine.testing.filters.FilterWithParameter;

import java.util.List;
import java.util.stream.Collectors;

public class MoreThanAnyTransferFilter implements FilterWithParameter<Flight, Long> {
    /*
    Группа фильтров, работающих с общей информацией о полёте:
    фильтр, возвращающий полёты, количество пересадок на которых не превышает N (упрощённый вариант ввода данных)
     */
    @Override
    public List<Flight> filter(Long count, List<Flight> flightList) {
        return flightList.stream().filter(
                flight -> flight.getSegments().size() < count + 1).collect(Collectors.toList());
    }
}
