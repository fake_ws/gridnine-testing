package com.gridnine.testing.filters.global;

import com.gridnine.testing.Flight;
import com.gridnine.testing.Segment;
import com.gridnine.testing.filters.FilterWithParameter;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class MoreThanAnyHoursOnFlightFilter implements FilterWithParameter<Flight, Long> {
    /*
    Группа фильтров, работающих с общей информацией о полёте:
    фильтр, возвращающий полёты, суммарное время которых с учётом пересадок составляет не более N часов (упрощённый вариант ввода данных)
     */
    @Override
    public List<Flight> filter(Long hours, List<Flight> flightList) {
        List<Flight> filteredList = new ArrayList<>();

        long time = 0;
        for (Flight flight : flightList) {
            List<Segment> segmentList = flight.getSegments();

            if (segmentList.size() == 1) {
                Duration duration = Duration.between(segmentList.get(0).getDepartureDate(), segmentList.get(0).getArrivalDate());
                time = Math.abs(duration.toHours());
            }

            if (segmentList.size() > 1) {
                Duration duration = Duration.between(segmentList.get(0).getDepartureDate(), segmentList.get(segmentList.size() - 1).getArrivalDate());
                time = Math.abs(duration.toHours());
            }

            if (time <= hours)
                filteredList.add(flight);
        }
        return filteredList;
    }
}
