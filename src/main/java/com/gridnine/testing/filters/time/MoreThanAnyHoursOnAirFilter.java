package com.gridnine.testing.filters.time;

import com.gridnine.testing.Flight;
import com.gridnine.testing.Segment;
import com.gridnine.testing.filters.FilterWithParameter;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class MoreThanAnyHoursOnAirFilter implements FilterWithParameter<Flight, Long> {
    /*
    Группа фильтров, работающих с информацией о нахождении пассажира в воздухе:
    фильтр, возвращающий полёты, суммарное время в воздухе на которых составляет не более N часов (упрощённый вариант ввода данных)
     */

    @Override
    public List<Flight> filter(Long hours, List<Flight> flightList) {
        List<Flight> filteredList = new ArrayList<>();

        for (Flight flight : flightList) {
            List<Segment> segmentList = flight.getSegments();

            long time = 0;
            for (int i = 0; i <= segmentList.size() - 1; i++) {
                Duration duration = Duration.between(segmentList.get(i).getDepartureDate(), segmentList.get(i).getArrivalDate());
                time += Math.abs(duration.toHours());
            }
            if (time <= hours)
                filteredList.add(flight);
        }
        return filteredList;
    }
}
